# lazytime
![](https://img.shields.io/badge/Python-3.8.6-green.svg)
![](https://img.shields.io/badge/pytz-2021.3-green.svg)

#### 介绍
基于python3的时间模块的懒人封包

#### 软件架构
软件架构说明


#### 安装教程
1.  pip安装
```shell script
pip3 install lazytime
```
2.  pip安装（使用淘宝镜像加速）
```shell script
pip3 install lazytime -i https://mirrors.aliyun.com/pypi/simple
```

#### 使用说明

1.  demo
```python
import lazytime
t = lazytime.get_datetime()
```